$('#product-slider-items').slick({
    dots: true,
    infinite: true,
    prevArrow: '#product-slider-prev',
    nextArrow: '#product-slider-next',
    appendDots: '#product-slider-dots',
});
