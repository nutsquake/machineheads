var bs = require('browser-sync').create();
var gulp = require('gulp');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var cleanCss = require('gulp-clean-css');

gulp.task('compress-css', function() {
	return gulp.src('./dist/css/*.css')
		.pipe(cleanCss())
		.pipe(gulp.dest('./dist/css/'));
});

/*gulp.task('compress-js', function() {
	return gulp.src('./dist/js/*.js')
		.pipe(uglify())
		.pipe(gulp.dest('./dist/js/'));
});

gulp.task('build-js', function() {
	return gulp.src(['./stylesheets/js/components/*.js', './stylesheets/js/main.js'])
		.pipe(concat('bundle.js'))
		.pipe(gulp.dest('./dist/js/'));
});*/

gulp.task('build-css', function() {
	return gulp.src('./stylesheets/main.scss')
		.pipe(sass({
			includePaths: ['./node_modules/']
		}).on('error', function(err) {
			console.error(err.message);
			bs.notify(err.message, 3000);
			this.emit('end');
		}))
		.pipe(gulp.dest('./dist/css/'))
		.pipe(bs.stream());
});

gulp.task('serve', function(done) {
	bs.init({ open: false, server: './dist/' });
	done();
});

gulp.task('watch', function() {
	// gulp.watch('./stylesheets/js/**/*.js', gulp.series('build-js'));
	gulp.watch('./stylesheets/**/*.scss', gulp.series('build-css'));
	gulp.watch(['./dist/*.html', './dist/js/*.js']).on('change', bs.reload);
});

gulp.task('default', gulp.series('serve', 'watch'));
